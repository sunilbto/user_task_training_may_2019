package com.inclusivityap.app;

import org.junit.Before;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.*;

import com.inclusivity.model.User;
import com.inclusivity.repo.UserRepository;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AppApplicationTests {

	@Value("${local.server.port}")
	private int serverPort;

	@Autowired
	UserRepository userrepo;

	@Before
	public void setUp() {
		RestAssured.port = serverPort;

	}

	@Test
	public void testToInsertUserScuccesfully() {
		String username = "AP";
		String firstName = "Pravin";
		String lastName = "Sanap";
		User user = new User(username, firstName, lastName);

		given().body(user).contentType(ContentType.JSON).when().post("/api/user").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("id", not(nullValue())).body("username", is(username))
				.body("first_name", is(firstName)).body("last_name", is(lastName));
		Optional<User> userOptional = userrepo.findByUsername(username);
		assertTrue(userOptional.isPresent());
	}
}
