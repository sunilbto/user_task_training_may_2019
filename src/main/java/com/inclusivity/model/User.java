package com.inclusivity.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Entity
@ToString
@NoArgsConstructor
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String username;
	@JsonProperty(value = "first_name")
	private String firstName;
	@JsonProperty(value = "last_name")
	private String lastName;
	public User(String userName, String firstName, String lastName) {
		this.username = userName;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	
}
