package com.inclusivity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.inclusivity.model.User;
import com.inclusivity.repo.UserRepository;

@RestController
@RequestMapping(value="/api/user", produces="application/json")
public class UserController {
	
	@Autowired
	UserRepository userrepo;
	
	@RequestMapping(method= RequestMethod.POST, consumes="application/json")
	public User addUser(@RequestBody User user) {
		System.out.println("Inside controller");
		return userrepo.save(user);
	}

}
